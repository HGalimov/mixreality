using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using System;

public class NetworkManagerHUD : NetworkBehaviour
{
	public NetworkManager manager;
	public GameObject start_server;
	public GameObject start_client;
	public GameObject start_host;
	public GameObject stop_host;
	public GameObject level;
	public InputField address;
	public InputField number_of_room;
	public bool room_is_selected = false;
	public string num_of_room;
	public bool isClnt = false;
	public int n;


	void Awake ()
	{		
		address.text = manager.networkAddress;
	}

	public void StartServer ()
	{
		if (!NetworkServer.active) {
			manager.StartServer ();
		}

	}

	public void StartClient ()
	{
		if (!NetworkClient.active) {			
			manager.StartClient ();
		}
	}

	public void StartHost ()
	{
		if (!NetworkClient.active && !NetworkServer.active) {			
			manager.StartHost ();
		}
	}

	public void StopHost ()
	{
		if (NetworkServer.active || NetworkClient.active) {
			manager.StopHost ();
		}
	}

	void Update ()
	{			
		if (!NetworkClient.active && !NetworkServer.active) {
			manager.networkAddress = address.text;
			address.text = manager.networkAddress;
			address.gameObject.SetActive (true);
			start_client.SetActive (true);
			start_server.SetActive (true);
			start_host.SetActive (true);
			stop_host.SetActive (false);
			level.SetActive (false);
			Destroy (GameObject.FindGameObjectWithTag ("Player"));
		} else {
			address.gameObject.SetActive (false);
			start_client.SetActive (false);
			start_server.SetActive (false);
			start_host.SetActive (false);
			stop_host.SetActive (true);
			level.SetActive (true);
		}
	}
}

