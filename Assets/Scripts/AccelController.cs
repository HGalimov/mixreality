﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public class AccelController : NetworkBehaviour
{

	public float vel_x;
	public float tmp_vel_x;
	public float vel_z;
	public float tmp_vel_z;
	public float svr_vel_x;
	public float svr_vel_z;
	public float acc_vel_x;
	public float acc_vel_z;
	public Rigidbody myRigidBody;
	public Vector3 acc;
	public List<float> a_x = new List<float> ();
	public List<float> a_x_f = new List<float> ();
	public List<float> a_y = new List<float> ();
	public List<float> a_y_f = new List<float> ();
	public List<float> a_z = new List<float> ();
	public List<float> a_z_f = new List<float> ();
	public float tm;
	public Text v;
	public Vector3 vel;
	GameObject obj;
	Camera cm;

	// Use this for initialization
	void Start ()
	{
		obj = GameObject.Find ("Camera");
		cm = obj.GetComponent<Camera> ();
		Input.gyro.enabled = true;
	}

	List<float> kalman_filter (List<float> acc, float k_stab)
	{
		List<float> acc_filtered = new List<float> ();
		acc_filtered.Add (0);
		for (int i = 1; i < acc.Count; i++) {
			var acc_f = k_stab * acc [i] + (1 - k_stab) * acc_filtered [i - 1];
			acc_filtered.Add (acc_f);
		}
		return acc_filtered;
	}

	// Update is called once per frame
	void Update ()
	{
		
		
		if (isLocalPlayer) {
			
			Input.gyro.enabled = true;
			acc = Input.acceleration;
			if ((int)Time.time <= tm + 1f) {
				a_x.Add (acc.x);
				//a_y.Add (acc.y);
				a_z.Add (acc.z);
				a_x_f = kalman_filter (a_x, 0.01f);
				a_z_f = kalman_filter (a_z, 0.01f);
			} else {
				tm = (int)Time.time;
				//v.text = a_x_f.Count.ToString () + " " + a_z_f.Count.ToString ();
				for (int i = 1; i < a_x_f.Count; i++) {
					tmp_vel_x += (a_x_f [i - 1] + a_x_f [i]) / 2f;
				}
				for (int k = 1; k < a_z_f.Count; k++) {				
					tmp_vel_z += (a_z_f [k - 1] + a_z_f [k]) / 2f;
				}

				acc_vel_x = tmp_vel_x;
				tmp_vel_x = 0;
				acc_vel_z = tmp_vel_z;
				tmp_vel_z = 0;
				a_x_f.Clear ();
				a_z_f.Clear ();
				a_x.Clear ();
				a_y.Clear ();
				a_z.Clear ();
			}


			if (Mathf.Abs (Input.gyro.userAcceleration.x) >= 0.1 || /*Mathf.Abs (Input.gyro.userAcceleration.y) >= 0.1 ||*/
				Mathf.Abs (Input.gyro.userAcceleration.z) >= 0.1)
					vel = transform.TransformDirection (cm.transform.forward);
			else
				vel = Vector3.zero;
			
			vel_x = vel.x * 5f/* Mathf.Abs (acc_vel_x)*/;
			vel_z = vel.z * 5f/* Mathf.Abs (acc_vel_z)*/;			
			Debug.Log (acc_vel_x);
			Debug.Log (acc_vel_z);
			if (SystemInfo.deviceType == DeviceType.Desktop) {				
				vel_x = Input.GetAxis ("Horizontal");
				vel_z = Input.GetAxis ("Vertical");
			}
			CmdMove (vel_x, vel_z);
			//v.text = vel_x.ToString () + "\n" + vel_z.ToString ();
			//vel_x = 0;
			//vel_z = 0;
		}
	}

	[Command]
	void CmdMove (float vel_x, float vel_z)
	{
		if (isServer) {
			svr_vel_x = vel_x;
			svr_vel_z = vel_z;
		}
	}

	void FixedUpdate ()
	{
		if (isServer) {
			myRigidBody.velocity = new Vector3 (svr_vel_x, 0, svr_vel_z);
			RpcUpdatePosition (transform.position);
			//RpcUpdateRotation (transform.rotation);				
		}
	}

	[ClientRpc]
	void RpcUpdatePosition (Vector3 pos)
	{
		if (isClient) {
			transform.position = pos;
		}
	}

	[ClientRpc]
	void RpcUpdateRotation (Quaternion rot)
	{
		if (this.isClient) {
			this.transform.rotation = rot;
		}
	}
}
