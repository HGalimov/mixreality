using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GyroController : MonoBehaviour
{
	private const float lowPassFilterFactor = 0.2f;
	private Quaternion calibration = Quaternion.identity;
	private Quaternion cameraBase =  Quaternion.identity;
	private Quaternion baseOrientation = Quaternion.Euler (90, 0, 0);
	private Quaternion baseOrientationRotationFix = Quaternion.identity;
	private Quaternion referenceRotation = Quaternion.identity;
	private readonly Quaternion baseIdentity =  Quaternion.Euler(90, 0, 0);
	public Quaternion gr;


	public Text txt;
	public float g_a;

	void Awake ()
	{
		Input.gyro.enabled = true;
		ResetBaseOrientation ();
		UpdateCalibration (true);
		UpdateCameraBaseRotation(true);
		RecalculateReferenceRotation ();
		transform.rotation = Quaternion.Slerp (transform.rotation, 
			cameraBase * (ConvertRotation (referenceRotation * Input.gyro.attitude) * GetRotFix()), lowPassFilterFactor);
	}

	void Update ()
	{
		gr = Input.gyro.attitude;
		transform.rotation = Quaternion.Slerp (transform.rotation, 
			cameraBase * (ConvertRotation (referenceRotation * Input.gyro.attitude) * GetRotFix()), lowPassFilterFactor);			
	}

	private void ResetBaseOrientation()
	{
		baseOrientationRotationFix = GetRotFix();
		baseOrientation = baseOrientationRotationFix * baseIdentity;
	}

	private Quaternion GetRotFix()
	{		
		return Quaternion.identity;
	}

	private void UpdateCalibration(bool onlyHorizontal)
	{
		if (onlyHorizontal)
		{
			var fw = (Input.gyro.attitude) * (-Vector3.forward);
			fw.z = 0;
			if (fw == Vector3.zero)
			{
				calibration = Quaternion.identity;
			}
			else
			{
				calibration = (Quaternion.FromToRotation(baseOrientationRotationFix * Vector3.up, fw));
			}
		}
		else
		{
			calibration = Input.gyro.attitude;
		}
	}

	private void UpdateCameraBaseRotation(bool onlyHorizontal)
	{
		if (onlyHorizontal)
		{
			var fw = transform.forward;
			fw.y = 0;
			if (fw == Vector3.zero)
			{
				cameraBase = Quaternion.identity;
			}
			else
			{
				cameraBase = Quaternion.FromToRotation(Vector3.forward, fw);
			}
		}
		else
		{
			cameraBase = transform.rotation;
		}
	}

	private static Quaternion ConvertRotation (Quaternion q)
	{
		return new Quaternion (q.x, q.y, -q.z, -q.w);	
	}

	private void RecalculateReferenceRotation ()
	{
		referenceRotation = Quaternion.Inverse (baseOrientation) * Quaternion.Inverse (calibration);
	}

}