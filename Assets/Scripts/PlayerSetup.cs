﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class PlayerSetup : NetworkBehaviour
{

	[SerializeField]
	Behaviour[] componentsToDisable;
	Ray ray;
	Ray ray2;
	Camera scene_camera;
	// Use this for initialization
	void Start ()
	{
		if (!isLocalPlayer) {
			for (int i = 0; i < componentsToDisable.Length; i++) {
				componentsToDisable [i].enabled = false;
			}
		} else {
			scene_camera = Camera.main;
			if (scene_camera != null) {
				scene_camera.gameObject.SetActive (false);
			}
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (SystemInfo.deviceType == DeviceType.Desktop) {
			ray = GameObject.FindGameObjectWithTag ("second").GetComponent<Camera> ().ScreenPointToRay (Input.mousePosition);
		}
		if (SystemInfo.deviceType == DeviceType.Handheld) {
			ray = GameObject.FindGameObjectWithTag ("second").GetComponent<Camera> ().ScreenPointToRay (Input.GetTouch (0).position);
		}
		RaycastHit hit;

		if (Physics.Raycast (ray, out hit, 3)) {
			if (hit.collider.gameObject.tag == "box" && Input.GetMouseButton (0)) {
				GameObject.Find ("Canvas").GetComponent<Switch_On> ().switch_on_b = true;
			}
		}
		if (Physics.Raycast (ray, out hit, 3)) {
			if (hit.collider.gameObject.tag == "frame" && Input.GetMouseButton (0)) {
				GameObject.Find ("Canvas").GetComponent<Switch_On> ().switch_on_f = true;
			}
		}
		if (Physics.Raycast (ray, out hit, 3)) {
			if (hit.collider.gameObject.tag == "lion" && Input.GetMouseButton (0)) {
				GameObject.Find ("Canvas").GetComponent<Switch_On> ().switch_on_l = true;
			}
		}
	}

	void OnDisable ()
	{
		if (scene_camera != null) {
			scene_camera.gameObject.SetActive (true);
		}
	}
}
