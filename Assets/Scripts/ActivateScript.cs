﻿using UnityEngine;
using System.Collections;

public class ActivateScript : MonoBehaviour
{

	// Use this for initialization
	void Start ()
	{
		if (SystemInfo.deviceType == DeviceType.Desktop) {
			GetComponent<GyroController> ().enabled = false;
		}
		if (SystemInfo.deviceType == DeviceType.Handheld) {
			GetComponent<Mouse> ().enabled = false;
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}
}
