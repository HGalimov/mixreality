﻿using UnityEngine;
using System.Collections;

public class Switch_On : MonoBehaviour {
	public GameObject box_qa;
	public GameObject frame_qa;
	public GameObject lion_qa;
	public GameObject qa;
	public GameObject var1_b;
	public GameObject var2_b;
	public GameObject var3_b;
	public GameObject var1_l;
	public GameObject var2_l;
	public GameObject var3_l;
	public GameObject var1_f;
	public GameObject var2_f;
	public GameObject var3_f;
	public GameObject quest_b;
	public GameObject quest_l;
	public GameObject quest_f;
	public GameObject disconnect;
	public bool switch_on = false;
	public bool switch_on_b = false;
	public bool switch_on_l = false;
	public bool switch_on_f = false;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (switch_on_b == true) {
			box_qa.SetActive (true);
			quest_b.SetActive (true);
			var1_b.SetActive (true);
			var2_b.SetActive (true);
			var3_b.SetActive (true);
			//disconnect.SetActive (false);
		}
		if (switch_on_b == false) {
			box_qa.SetActive (false);
			//disconnect.SetActive (true);
		}

		if (switch_on_l == true) {
			lion_qa.SetActive (true);
			quest_l.SetActive (true);
			var1_l.SetActive (true);
			var2_l.SetActive (true);
			var3_l.SetActive (true);
			//disconnect.SetActive (false);
		}
		if (switch_on_l == false) {
			lion_qa.SetActive (false);
			//disconnect.SetActive (true);
		}

		if (switch_on_f == true) {
			frame_qa.SetActive (true);
			quest_f.SetActive (true);
			var1_f.SetActive (true);
			var2_f.SetActive (true);
			var3_f.SetActive (true);
			//disconnect.SetActive (false);
		}
		if (switch_on_f == false) {
			//disconnect.SetActive (true);
			frame_qa.SetActive (false);
		}		
	}
}
