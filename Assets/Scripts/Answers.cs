﻿using UnityEngine;
using System.Collections;

public class Answers : MonoBehaviour {
	public GameObject var1;
	public GameObject var2;
	public GameObject var3;
	public GameObject question;
	public GameObject answ;
	public GameObject ok;
	public bool is_answered = false;
	public bool is_clicked_ok = false;
	//public GameObject qa;

	// Use this for initialization
	void Start () {
	
	}

	public void Answer () {
		is_answered = true;
	}

	public void Click_Ok () {
		is_clicked_ok = true;
	}
	 
	// Update is called once per frame
	void Update () {
		if (is_answered == true) {
			question.SetActive (false);
			var1.SetActive (false);
			var2.SetActive (false);
			var3.SetActive (false);
			answ.SetActive (true);
			ok.SetActive (true);
		}
		if (is_clicked_ok == true) {
			GameObject.Find ("Canvas").GetComponent<Switch_On> ().switch_on_l = false;
			GameObject.Find ("Canvas").GetComponent<Switch_On> ().switch_on_b = false;
			GameObject.Find ("Canvas").GetComponent<Switch_On> ().switch_on_f = false;
			answ.SetActive (false);
			ok.SetActive (false);
			is_answered = false;		
			is_clicked_ok = false;
		}
	}
}
