﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Linq;
using System;

public class Pazl : MonoBehaviour
{
	public GameObject green_coin;
	public GameObject red_coin;
	public GameObject yellow_coin;
	public List<GameObject> coins = new List<GameObject> ();
	public List<GameObject> bunch_of_coins = new List<GameObject> ();
	public List<GameObject> all_coins = new List<GameObject> ();
	public System.Random rnd = new System.Random ();
	public float radius;
	public GameObject entry;
	Ray ray;
	// Use this for initialization
	void Start ()
	{
		coins.Add (green_coin);
		coins.Add (red_coin);
		coins.Add (yellow_coin);
		for (float x = -61.72f; x <= -58.32f; x += 0.42f) {
			for (float y = 2.14f; y >= -0.14f; y -= 0.42f) {
				all_coins.Add (Instantiate (coins [rnd.Next (0, coins.Count)], new Vector3 (x, y, 3.5f), Quaternion.identity) as GameObject);
			}
		}
	}

	void Drawing_list_of_coins (GameObject touch)
	{
		Collider[] hitColliders = Physics.OverlapSphere (touch.transform.position, 0.4f);
		for (int i = 0; i < hitColliders.Length; i++)
			if (touch.GetComponent<Collider> ().tag == hitColliders [i].tag) {
				if (!bunch_of_coins.Contains (hitColliders [i].gameObject)) {
					bunch_of_coins.Add (hitColliders [i].gameObject);
					Drawing_list_of_coins (hitColliders [i].gameObject);
			}
		}

	}
	
	// Update is called once per frame
	void Update ()
	{
		if (SystemInfo.deviceType == DeviceType.Desktop) {
			ray = GameObject.FindGameObjectWithTag ("second").GetComponent<Camera> ().ScreenPointToRay (Input.mousePosition);
		}
		if (SystemInfo.deviceType == DeviceType.Handheld) {
			ray = GameObject.FindGameObjectWithTag ("second").GetComponent<Camera> ().ScreenPointToRay (Input.GetTouch (0).position);
		}
		RaycastHit hitInfo;
		if (Physics.Raycast (ray, out hitInfo, 5f)) {			
			if (hitInfo.collider && (hitInfo.collider.gameObject.tag == "green" || hitInfo.collider.gameObject.tag == "red"
			    || hitInfo.collider.gameObject.tag == "yellow") && Input.GetMouseButton (0)) {			
				Drawing_list_of_coins (hitInfo.collider.gameObject);

			}
		}
		if (bunch_of_coins.Count >= 3) {
			foreach (GameObject coin in bunch_of_coins) {
				all_coins.Remove (coin);
				Destroy(coin.GetComponent<SphereCollider>());
				Destroy(coin.GetComponent<Rigidbody>());
				coin.transform.position = Vector3.Lerp (coin.transform.position, entry.transform.position, 1);
				Destroy (coin, 0.5f);
			}
		}
		bunch_of_coins.Clear ();
	}
}
